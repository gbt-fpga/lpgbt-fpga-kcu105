# LpGBT-FPGA IP example design for KCU105 (core version 2.2.0)

The LpGBT ASIC (Low Power GigaBit Transceiver) is a new 65nm-CMOS radiation tolerant serializer/deserializer 
device that can be used on the front-end electronics of the HL-LHC detectors. This component is foreseen to 
be used by CMS and ATLAS for their system upgrades and offers a set of encoding and decoding schemes specifically 
tailored to meet their needs in terms of radiation-hardness and data bandwidth.

The LpGBT-FPGA project started in 2018 as a natural evolution of the existing GBT-FPGA to provide a back-end 
counterpart to the future LpGBT ASIC. The new FPGA IP implements the encoding/decoding schemes supported by 
the front-end ASIC, meaning that it can be configured using 4 different combinations for the upstream link 
(from Front-end to Back-end): two decoding schemes (FEC5 or FEC12) based on Reed-Solomon techniques to configure 
the encoding robustness and two line rates (10.24 or 5.12Gbps) depending on the required bandwidth. Additionally, 
the LpGBT-FPGA core features an asymmetric architecture to match the LpGBT ASIC specificities: the transmitter 
part of the IP, as opposed to the configurable receiver part, proposes a single encoding scheme (FEC12) and a 
lower line rate of 2.56Gbps. Such an asymmetry prevents the IP to be used in loopback mode for self-testing.

<div style="border: 1px solid #faebcc; background:#fcf8e3; color:#8a6d3b; padding: .75rem 1.25rem; border-radius: .25rem;"><b>Warning:</b> Philosophy changed since the GBT-FPGA: the LpGBT-FPGA is not 
anymore given as a generic module that can be implemented in one block. It is now proposed as a set of modules with implementation example and reference notes 
to help the user in designing its own system in the most efficient way. **Example design can be subject to changes without notice as necessary.**</div>

## Links

- [Gitlab repo - LpGBT-FPGA IP](https://gitlab.cern.ch/gbt-fpga/lpgbt-fpga)
- [Documentation](http://lpgbt-fpga.web.cern.ch/doc/html/)
- [LpGBT Sharepoint] (https://espace.cern.ch/GBT-Project/LpGBT/default.aspx)

## Repository architecture

* **LpGBT-FPGA**: This folder contains the VHDL files that describe the logic of the different modules required to implements the IP (Encoder/Decoder, Scrambler/Descrambler, Gearboxes, Frame aligner)
* **cdc_user**: This folder contains an example of how to implement a clock-domain-crossing for 40MHz to 320MHz (with clock-enable) for fixed-latency applications. 
* **Mgt**: This folder contains the Mgt module used to serialize/deserialize the data. In the case of the KCU105 design, it contains a GTH Transceiver (Xilinx).
* **TCL**: This folder contains the TCL script that defines the procedure to compile and simulate the project
* **Constraints**: IO and timming constraints
* **hdl**: VHDL sources of example design's files (LpGBT-FPGA top level, Stimulis, ...).
* **lpgbt-fpga-kcu105_quick_start.pdf**: Quick start-guide explaining the basic steps to make this example design work and how to initialize the setup. 

## How-to

The vivado folder includes two projects:

 - lpgbt-fpga-kcu105-10g24: configured to work with an uplink running at 10g24.
 - lpgbt-fpga-kcu105-5g12: configured to work with an uplink running at 5g12.

> **Tip:** By default, the design are configured to work in FEC5 mode. To compile it for FEC12, you have to change the value of the FEC generic in the `lpgbtfpga_kcu105_10g24_top` or `lpgbtfpga_kcu105_5g12_top` file to `FEC12`.

> **Tip:** Before using the example designs, the users have to update the lpgbt-fpga submodule using the following terminal commands:
> ```
> cd lpgbt-fpga
> git submodule init
> git submodule update
> ```
> Or  using tortoise git:
> ```
> right click > TortoiseGit > Submodule Update
> ```

## KCU105 testbench

The KCU105 test bench is used to validate the routing feasibility of the LpGBT-FPGA. Especially from the timing point of view. Additionally. it used to qualify the IP by measuring the coding gain and latency.

<img src='http://lpgbt-fpga.web.cern.ch/img/kcu105BlockDiagram.png' />
